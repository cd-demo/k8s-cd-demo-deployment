const fs = require('fs');
const versionsString = fs.readFileSync('versions.json');

const appName = process.argv[2]
const versions = JSON.parse(versionsString)

console.log( versions[appName].lockedVersion || versions[appName].version )
