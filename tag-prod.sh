#!/bin/bash
set -e

git checkout -b master
git pull origin master

TAG=$(echo $(git tag -l --sort=-v:refname | head -n 1) | tr "-" "\n" | head -n 1)

TAG=$TAG-prod

echo applying tag: $TAG

git config --global user.name "$GITLAB_USER_ID"
git config --global user.email "$GITLAB_USER_EMAIL"
git tag $TAG -m "automatic release after successful systemtest"
git push --tags $CI_REPOSITORY_URL master