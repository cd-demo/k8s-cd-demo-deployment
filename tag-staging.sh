#!/bin/bash
set -e

git checkout master
git pull origin master

echo checking if tagging staging is needed ...

if [[ $(git tag -l --points-at HEAD) ]]; 
    then 
        echo "... no need to tag ..."
        exit 0 
fi

TAG=$(echo $(git tag -l --sort=-v:refname | head -n 1) | tr "-" "\n" | head -n 1)

echo latest tag is: $TAG

let TAG+=1
TAG=$TAG-staging

echo new tag is: $TAG
echo "... applying tag ..."
git config --global user.name "$GITLAB_USER_ID"
git config --global user.email "$GITLAB_USER_EMAIL"
git tag $TAG -m "applied by scheduled pipeline"
git push --tags $CI_REPOSITORY_URL master