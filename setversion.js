const fs = require('fs');
const versionsString = fs.readFileSync('versions.json');

const appName = process.argv[2]
const newVersion = process.argv[3]
const versions = JSON.parse(versionsString)

versions[appName].version = newVersion

fs.writeFileSync('versions.json', JSON.stringify(versions, null, 2))  